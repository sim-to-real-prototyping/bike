# Importing Libraries
import serial
import time
arduino = serial.Serial(port='/dev/ttyACM0', baudrate=115200, timeout=.1)


def write_read(x):
    arduino.write(bytes(x, 'utf-8'))
    time.sleep(0.05)
    data = arduino.readline()
    return data
while True:
    num = input("Enter a number: ") # Taking input from user
    value = write_read(num)
    print(value) # printing the value

"""
try: 
  arduino = serial.Serial("/dev/ttyACM0", baudrate=115200, timeout=1) # Ei toiminut ehkä koska baudrte puuttui
except:
  print("usb connection failed")

rawdata = []
count = 0
while count < 3:
  #rawdata.append(str(arduino.readline()))
  print(arduino.readline())
  count += 1
"""