import pybullet as p
#from physics_simulator import Physics
import random
import numpy as np

class RandomForce:
  def __init__(self, target_link, vehicle_id, location_from_center, force, duration):
    self.vehicle_id = vehicle_id
    self.counter = duration
    self.target_link = target_link
    self.location_from_center = location_from_center
    self.force = force

  def step(self, new_step):
    if self.counter < 0:
      raise Exception("Illegal random force step. Random force has already ended")

    if new_step:
      self.counter -= 1
    p.applyExternalForce(self.vehicle_id, self.target_link, self.force, self.location_from_center, p.LINK_FRAME)

    done = self.counter <= 0
    return done

class RandomForces:
  def __init__(self, links, vehicle_id, include_randome_forces):
    self.vehicle_id = vehicle_id
    self.include_randome_forces = include_randome_forces
    self.links = links
    self.forces = []
    self.new_force_every_n_steps = 1 #10
    self.max_force = 100 # 150
    self.max_duration = 10 #20
    self.max_dist_from_center = 1

  @staticmethod
  def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    if np.absolute(vector).sum() < 1e-10:
      print("Warning unit vector taken from zero vec. Small epsilon added to avoid exceptions")
      return vector / np.linalg.norm(np.array(vector) + 0.000001)
    return vector / np.linalg.norm(vector)


  def _forces_step(self, obj):
    done = obj.step(True)
    return not done

  def render(self):
    [a.step(False) for a in self.forces]

  def step(self):
    if not self.include_randome_forces:
      return

    create_new_force = lambda: random.random() < 1/self.new_force_every_n_steps

    if create_new_force():
      rnd_index = random.randint(0,len(self.links)-1)
      target_link = self.links[rnd_index]

      location_from_center = [random.random() for _ in range(3)]
      location_from_center = self.unit_vector(location_from_center) * self.max_dist_from_center

      force = [random.random() for _ in range(3)]
      force = self.unit_vector(force) * self.max_force

      duration = int(random.random()*self.max_duration)

      self.forces.append(RandomForce(target_link, self.vehicle_id, location_from_center, force, duration))

    self.forces = [a for a in filter(self._forces_step, self.forces)]

