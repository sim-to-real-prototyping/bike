#from env import BikeGym
import numpy as np
import time

from sim.keyboard_actions import KeyboardActions
from sim.controller import Controller
from sim.envs.env import BikeGym
from sim.driver import NoDriver


mallilla = True

# TODO: 
#lisää paino jonka paikkaa muuttamalla voi kalllistaa ajoneuvoa
#-mallin ulostulo on korjausliike, eikä absoluuttinen
#- muuta rewardia
# takarenkaan pyörimisnopeus on yksi mahdollinen muuttuja. Auttaisi nopeuden kanssa päättelemään sutiiko rengas. 
# * miten eturenkaan luisto?
# Sama episode ees ja taas
# hitailla nopeuksilla kallistus ei toimi

# lisää randome force include_randome_forces
#pienemmä max/min kulmaa
# epoch määrä 100
# episodi kohtaisia randomeja konfiguraatioita. Lisää randomea:
# * kitkaan, 
# * renkaiden kokoon
# * rungon kappaleiden painoon
# * rungon kappaleiden kokoon
# * rungon osien kiinnityskohtiin "joint"
# * inertialiin
# Mikäli ei opi hyvin niin kokeile erilaisia neuroverkko konfiguraatioita.
# * kallistuskulma nopeuden parametrinä?
# * jonkinlainen kääntymistä ilmaiseva parametri stateen ja reward laskentaan?

#Jäin siihen, että kokeile saada kääntymään tosi hitaissa nopeuksissa. 
#* ehkä voisin vai tuoda alinta mahdollista treeninopeutta alaspäin ja laskea ylimmän. esim [40,300] 

# TODO: https://ai.googleblog.com/2021/06/toward-generalized-sim-to-real-transfer.html

"""
Ota esimerkkiä filestä urdf_util.py
funktionaalisuus lisätään pybullet_util funktioon _set_pybullet_bodies
Lue urdf
etsi tietty komponentti
ota arvo
generoi randomea muutosta  min= -1/20*arvo, max= 1/20*arvo
lisää generoitu arvo alkuperäiseen arvoon
tallenna urdf pohjalta tehtyyn malliin.
"""

"""
jäin siihen, että kallistuskulma ei yksin ole riittävä hitaissa nopeuksissa
varmista että nopeuden normalisointi menee hyvin
Ota myös force inputiksi

ONGELMA: Ei toimi fyysisessä robotissa. 
user command voisi olla millaista ympyrää kulkemaan.
* laske ympyrälle keskipiste, etäisyydellä r bikestä (x,y)
* x ajan jälkeen laske biken etäisyys d keskipisteestä (x,y)
* penalty on r - d


ONGELMA: miten kaikki töyssyt ja hypyt ja tärinät ja kallistuessa kiihtyvyys ylöspäin 
laske robotin kiihtyvyys
* laske nopeusvektori ajanhetkellä t
* laske nopeusvektori ajanhetkellä t + 1
* laske kiihtyvyys vektori
* penalty on target kiihtyvyys miinus todellinen
** target kiihtyvyyden suunta tulee user actionilta ja suuruus päätellään nopeudesta?
* jos kaatuu niin mitä tapahtuu?

Aito tarve on saada pyörän runko kääntymään toiseen asentoon
* fyysinen laite
** voi hyödyntää magnetosmetriä
** voi ehkä hyödyntää kulmamuutosten integraalia
** kalman filter?
* simulaattori
** user input määrittää paljonko yaw pitää olla tietyssä ajassa. 
** eri nopeudet? ei erityistä kohtelua
"""

# https://www.instructables.com/Accelerometer-Gyro-Tutorial/


if mallilla:
  start = time.time()
  Controller().run()
  end = time.time()
  print("Run took", end - start)
else:
  env = BikeGym(NoDriver(), False, True, "UX", include_randome_forces=False)
  env.reset()
  
  #env = BikeGym(False, True)
  forward_force = 1
  turn_right = 0
  count = 0
  #time.sleep(4.02)

  while count < 5000:
    count += 1
    target_p = [None]

    parced = KeyboardActions.list_keyboard_actions()

    target_p = np.array(target_p)
 
    if "right" in parced:
      turn_right += 0.02
    elif "left" in parced:
      turn_right -= 0.02
    target_p[0] = turn_right
    target_p = np.clip(target_p, -1,1)
 
    time.sleep(0.02)
    if count%5 == 0:
      env.move_camera(env.vehicle_id, True)
    o, r,d, i = env.step(target_p)

    
#episodi: arvo tavoitila episodin alussa(rakentjassa ja resetissä). jos alle 0.1 päässä tarpeeksi kauan niin 
#  episode ratkaistu. Kauan?
#reward: penalty siitä kuinka kaukana on tavoitetilasta.