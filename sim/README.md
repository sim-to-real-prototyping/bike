```
cd projects/bike
conda activate first_robot2
python run.py
```


# 3D printtaus
* sta K-Raudasta
** lasta 40mm
** laakerit
** työkalupakki/lokerikko
*** https://www.k-rauta.fi/tuote/tyokalupakki-stanley-1-94-210-pyorilla-liikuteltava/3253561942105
** viila

https://ai.googleblog.com/2021/06/learning-accurate-physics-simulator-via.html

# testi logit:

* jos ohajuskulma -1,1 ja force 10 - 300, niin ei meinannut oppia mitään
* kun vein ääripäitä välille 0.3 - 0.7 ääripäistä, niin oppi paljon paremmin. Edelleen heikosti ja käyttäytyi oudosti kun antoi ohajussignaaleja.


* ensimmäinen on dist ajo suuremmalla dist kertoimella - ei auttanut eikä haitannut
* toinen testaa nopeutuuko oppiminen jos update_times tulpaantuu - ehkä oppi vähän vähemmillä stepeillä, mutta kesti reilusti kauemmin.
* testaaa niin, että reward oikealla gravitation suunnalla. - oppii mutta lopputulos on huono.
* anna oikea kallistus inputtina. varmista että silleen toimii
* tein uuden estimaten jossa 0.1 estimatea ja 0.9 todellista grav suuntaa -> oppi about yhtä hyväksi kuin ilman 0.1 estimatea, mutta kesti huomattavasti kauemmin oppia
* lisäsin grav suuruuden. ehkä aavistuksen huonommin?
* tein uuden estimaten jossa 0.5 estimatea ja 0.5 todellista grav suuntaa -> oppii paljon huonommin
* sama kuin äsken, mutta ilman dist. Aiemmin distillä oppi hitaammin. Entä nyt? -> oppii reilusti paremmin kuin distillä
* laske painotettu keskiarvo acceleration suunnalle (ensimmäinen ajo on painotettu, jälkimmäinen on ei painotettu vertailu). self.acceleration_avg, kerroin 0.1 -> painotettu näyttäisi oppivan vähän paremmin, mutta huonosti
* _update_estimation_vec: muuta estimate kerroin 15:sta 30:een. -> alkoi oppia nopeammin, mutta lopulta paras arvo jäi huonommaksi
* _update_estimation_dist_vec muutettu lisäys termi 0.5 1:een. Lisäksi env normalisation laskennassa default estimation sijaan dist -> alkoi oppia nopeammin, mutta lopulta paras arvo jäi huonommaksi
* anna sekä dist että että default grav_vec rollinputiksi. Default sisältää historiaa ja dist pelkkä viimeisin. _normalize_state lisätty
* normi default roll mutta kaksi historiatietoa. Ensimmäisessä roll_change_earlier1 ja toisessa roll_change_earlier2 -> kun 1 niin paljon parempi. Kuna 2 niin ehkä vähän parempi
* ilman 1&2 - alussa oli paras, sitten ei ollut paras. Kun annoin ajautua paljon pidempään kuin muut niin oppi varsin hyvin. * kokeile uudelleen roll_change_earlier1. Anna ajautua vähintään 600k steppiä -> oppi muttei niin hyvin kuin ilman .

* muutettu niin että pyrkii saamaan nopeasti kiihtyvyyden jompaan kumpaan roll suuntaan ja sitten vakiinnuttaa kiihtyvyyden nollaan
* testaa missä reward lasketaan target-acceleration erotus potenssiin kaksi -> ei toiminut
* sama kuin yllä, mutta ei potenssia -> ei toiminut
* kokeile ilman acceleration_avg:tä -> ei opi
* vertaa accelerationia estimatioon. Kokeile estimaatioita eri w painoarvoilla -> ei opi
* roll sijaan vertaile mitä local y ja z arvoja saa. Tämä osaa ehkä ottaa huomioon jos kaatuu lähes vapaassa pudotuksessa. Target voisi vaikka olla mitä y arvoja saa. tai mitä y/x arvoja saa -> ei opi
* sama kuin yllä mutta inputissa y ja z arvot kerrottu kahdella -> ei opi

### USEAMPI OSAINEN MALLI ### 
* treenaa ensin moottorikomento malli. 
** input: absoluttinen y arvo. Ehkä z. nopeus, torque, ohjaustangon asento, painotettu y-muutos. ohjaustanko muutos. target z
** reward: miten lähellä seuraavan stepin y on target y:tä.
* jos oppii niin koita muuttaa moottorikomennot probabilistisiksi
* jos saan tämän toimimaan, niin tee korkemman tason ohjaus kontrolleri.
** input: imu arvot. Päättele näistä missä on alas + käyttäjän komento. Esim kuinka paljon pitää kääntyä -> Päättele näistä pitääkö kääntyä  [vasen, ei mihinkään, oikea]

Logging:
* reward input lasketaan gryo.roll arvosta. controller saa gryo rollin inputiksi. 
** kun estimation y&z mukana inputissa -> kaatui suoraan kyljelleen, jonka jälkeen penalty meni noin nollaan. Siis kosksa muutos tavoite on nolla ja kyljellään muutos on nolla 
*** lisää penalty jos y liian iso --> oppii hyvin. Pystyy jopa jossain määrin ohjaamaan vaikka ei erikseen treenattu sitä varten.
** kun estimation y&z EI mukana inputissa -> parhaan rewardin malli oppi ajamaan kohtisuoraan eteenpäin. Viimeisin (3h kohdalla) lähti kaartamaan pikkuhiljaa, yritti korjata ja kaatui toiselle kyljelle
* sama kuin ylempi joka oppi z ja y tiedoilla, mutta target y voi muuttua -> oppi tosi hyvin. -Olikin bugi. Ei y muuttunut
* sama kuin edellä mutta target decay hitaampi ja max y 0.2 -> 0.3
* Lisää prev_roll _normalize_state methodissa
* act_noise test enviin
* tee logiikka joka kääntelee pyörää edes takaisin. 
** esim ensin laittaa rolliksi -0.8, sitten kun absoluuttinen kulma on 20deg, niin laittaa rollksi nolla. Pidä hetki paikallaan. Sitten roll 0.4 ,kunnes kulma on 10deg. pysäytä ja jatka kunnes kulma on -10deg
** max/min kulma. ota randomesti uusi. Pidä hetki ja valitse uusi. 
** voiko dr_wheel_change ja dr_wheel poistaa inputeista?
* ensimmänen ajo reward roll_penalty expotentiaalinen 1.5
* toinen ajo, lineaarinen -> tämä oli parempi
* kokeile replay_size = int(1e5), koekile self.simulations_per_step = 7. Näiden tarkoitus on koittaa nopeuttaa oppimista. -> replay_size 1e5 oli huonompi.


* lisää randomea actioniin myös demossa.



#### Voiko td3 muuttaa stokastiseksi?
* periaatteessa on jo kun act_noise > 0
* etsi "def mlp(", niin näet miten policy tehty
* ehkä noise ajaa stokastisen aseman.




### ###

* testaa gryolla seuraavaa: laske oikea grav_vec. Aina jos kallistuu jompaan kumpaan suuntaan niin anna käsky kääntää vastakkaiseen. Reward gryon rollista. reward sen mukaan kääntyikö oikeaan suuntaan.
* voiko palauttaa arvon sijaan samplatun arvon? Kokeile sitten kun saat jonkun toimimaan

* oppisiko gryolla? voisi yrittää pitää gryoa nollassa?
* kokeile samaa mutta anna oikea gravity koordinaatti. varmista että oppii edes oikeilla. Muuta reward.
* kokeile tuunata rollia niinkuin aiemmin, jotta pienet rollit korostuvat
* voisi kokeilla, että päätän tavoite rollin. Sitten driver saa oikean grav_vec josta päättelee kumpaan suuntaan pitää kääntää. Controllerin tehtävä on käääntää siihen suuntaan. penalty sen mukaan miten kaukana on tavoitteesta VAI kääntyikö oikeaan suuntaan? 

TODO: 
* mallin ulostulo korjaukseksi annettuun komentoon eikä komento.
* mallin ulostulo korjaukseksi nykytilaan estämään kaatumista eikä komento.
** tarvitseeko komennon olla inputtina? Ehkä tarvitsee, niin mallilla enemmin aikaa reagoida.
* muuta nykyinen reward gae:ksi
* millä kertoimella nykyinen rewad vähenee? Eli kuinka paljon vanhat rewardit vaikuttaa?


Inertial measurement unit

* Voisin ostaa kaksi Silicon MEMS -> ehkä vähentää noisea
* voisin ostaa FOG IMUn. RLF IMUt vielä tarkempia mutta kalliimpia ja isompia
* pystynkö lukemaan arduinolla?
*  If an IMU is manufactured with temperature compensation, this will improve the stability of the measurements.
* In general, inertial sensors can be grouped into performance classes according to bias stability specifications which help to categorise the performance of a module. The higher the grade of sensor, the more accurate it tends to be.
* Tactical-grade IMUs vs Industrial-grade IMUs vs navigation-grade, vs strategic or military-grade.
* IMUs with low vibration sensitivity provide higher accuracy navigation solutions in harsh UAV applications where vibration levels are high.
* SBG Systems saattaisi tehdä hyviä

https://www.unmannedsystemstechnology.com/feature/selecting-an-inertial-measurement-unit-imu-for-uav-applications/

