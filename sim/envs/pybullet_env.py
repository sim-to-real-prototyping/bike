from easydict import EasyDict
import pybullet as p
import numpy as np
import math
import time
from sim.random_force import RandomForces
from sim.vehicle_info import VehicleInfo
from sim.envs.pybullet_timespace import PybulletTimespace, IMU
from sim.direction_calculations import DirectionEstimator


class PyBulletEnv:
  def __init__(self):
    self.gravity_f = 9.81
    self.view_state = EasyDict({ "prev_xy":None})
    self.camera_link_id = 3
    self.simulations_per_step = 5
    self.steps_per_sec = 240
    self.ccounter = 0
    self.time_interval_length_s = self.simulations_per_step/self.steps_per_sec

  def init_after_pybullet(self, include_randome_forces):
    self.randome_forces = RandomForces(self.vehicle_info.link_ids, self.vehicle_id, include_randome_forces)
    self.spatial = PybulletTimespace(self.vehicle_info, self.vehicle_id, self.time_interval_length_s, self.gravity_f)
    self.direction_estimates = DirectionEstimator(self.spatial, self.vehicle_info)
    self.imu = IMU(self.spatial)

  def setUpPyBullet(self, gui, include_randome_forces):
    if gui:
      p.connect(p.GUI)
    else:
      p.connect(p.DIRECT)
    
    #roll,pitch,yaw = 0,0,0 #1.4,0,0 # roll pitäisi olla pitch?
    p.configureDebugVisualizer(p.COV_ENABLE_MOUSE_PICKING, 1)
    p.setRealTimeSimulation(0)

    plane = p.createCollisionShape(p.GEOM_PLANE)
    p.createMultiBody(0, plane)

    # TODO: näitä ei tarvii luoda joka resetin yhteydesssä
    p.configureDebugVisualizer(p.COV_ENABLE_GUI, 0)
    p.configureDebugVisualizer(p.COV_ENABLE_RENDERING, 1)
    p.configureDebugVisualizer(p.COV_ENABLE_KEYBOARD_SHORTCUTS, 0)
    p.setGravity(0, 0, -self.gravity_f)

    p.configureDebugVisualizer(p.COV_ENABLE_MOUSE_PICKING, 1)
    #p.configureDebugVisualizer(p.COV_ENABLE_GUI, 0)
    # SHortcurs removed, so that keyboard can be used to drive a plane.
    #p.configureDebugVisualizer(p.COV_ENABLE_PLANAR_REFLECTION, 1)

    self._set_pybullet_bodies(gui)
   
    if not gui:
      p.removeAllUserDebugItems()

    self.init_after_pybullet(include_randome_forces)

  def move_camera(self, vehicle_id, update):
    state = p.getLinkState(vehicle_id, self.camera_link_id)
    world_pos_center, world_ori_center = [state[i] for i in [0,1]]
    pitch,roll,yaw = p.getEulerFromQuaternion(world_ori_center)

    wx,wy,_ = world_pos_center
    if self.view_state.prev_xy is not None:
      wxp,wyp = self.view_state.prev_xy
      dx = wx - wxp
      dy = wy - wyp
      xy = self.spatial.unit_vector([dy,dx])
      rad = math.atan2(xy[0], xy[1]); 
      yaw = rad - math.pi / 2

    self.view_state.prev_xy=(wx,wy)
      
    yaw = yaw / math.pi*180
    outp = p.getDebugVisualizerCamera()
    dist = outp[10]

    x,y,z = world_pos_center

    if update:
      act = self.driver.action
      po = self.prev_obs[-1]
      roll = DirectionEstimator.get_gravitation_roll(po.estimations.default)
      roll = np.round(roll, 2)
      dr_wheel = np.round(po.dr_wheel, 2)
      speed = np.round(po.speed, 2)
      text = f"force: {act.force}, roll {roll}, dr_wheel {dr_wheel}, speed {speed}"
      p.removeAllUserDebugItems()
      p.addUserDebugText(text, [x,y-1,z+1.5], [0,0,0])

      outp = p.getDebugVisualizerCamera()
      yaw = outp[8]
      pitch = outp[9]
      dist = outp[10]

      p.resetDebugVisualizerCamera(dist,yaw,pitch,(x,y,z+1.5))
  
  def _set_pybullet_bodies(self, init=True):
    self.vehicle_id = p.loadURDF("sim2real.urdf")

    self.vehicle_info = VehicleInfo(self.vehicle_id)

    p.setJointMotorControl2(self.vehicle_id, self.vehicle_info.acceleration_joint_id, p.VELOCITY_CONTROL, force=0)
    p.setJointMotorControl2(self.vehicle_id, self.vehicle_info.front_wheel_joint_id, p.VELOCITY_CONTROL, force=0)

    x,y,z = 0,0,1.1
    roll,pitch,yaw = 0,1.57,0 #1.4,0,0 # roll pitäisi olla pitch?
    q=p.getQuaternionFromEuler((roll,pitch,yaw))
    p.resetBasePositionAndOrientation(self.vehicle_id,(x,y,z),q)




  # def _get_roll(self, estimation):
  #   roll = self.direction_estimates.get_roll(estimation)
  #   return roll

  def get_roll_old_remove(self):
    # Returns roll for reward calculation etc. Roll calculation has direct access to pybullet simulator
    li = self.vehicle_info.link_info
    center_world_pos_center = self.spatial.link_world_position(self.vehicle_info.measurement_point)
    right_world_pos_center = self.spatial.link_world_position(self.vehicle_info.measurement_point_right)
    z_diff = center_world_pos_center[2] - right_world_pos_center[2]
    z_diff_sign = np.sign(z_diff)
    z_diff = abs(z_diff)
    z_diff /= 0.05
    z_diff = z_diff_sign*z_diff
    #z_diff =  z_diff_sign*math.sqrt(z_diff)
    return z_diff

  def obs(self):
    world_pos_center = self.spatial.link_world_position(self.vehicle_info.measurement_point)

    imu_vals = self.imu.values()
    estimations, acc_quantity = self.direction_estimates.force_estimations(imu_vals,self.time_interval_length_s)
    #gravitation_direction = -estimations.default
    driving_wheel_pos = p.getJointState(self.vehicle_id, self.vehicle_info.driving_wheel_joint_id)[0]

    return estimations, acc_quantity, world_pos_center, driving_wheel_pos

  def reset_pybullet(self):
    p.removeBody(self.vehicle_id)
    self._set_pybullet_bodies(False)
    return self.next_observation()

  def get_speed(self, pos, pos_prev):
    # NOTE: pos is used for speed calculation. To be more accurate representation of reality, should speed be calculated based on wheel size and spinning speed 
    dist = math.hypot(pos[1] - pos_prev[1], pos[0] - pos_prev[0], pos[2] - pos_prev[2])
    return dist*self.steps_per_sec/self.simulations_per_step

  def next_observation(self):
    self.spatial.save_state()
    _, _, pos_prev, _ = self.obs()
    for _ in range(self.simulations_per_step):
      p.stepSimulation()

    estimations, acc_quantity, pos, dr_wheel = self.obs()
    
    speed = self.get_speed(pos, pos_prev)
    obs = EasyDict({"estimations":estimations, "acceleration_quantity": acc_quantity, "speed":speed, "dr_wheel":dr_wheel})

    return obs
    
  def _take_action(self, action, target):
    p.setJointMotorControl2(self.vehicle_id,self.vehicle_info.acceleration_joint_id,p.TORQUE_CONTROL,force=target.force) # target.force
    p.setJointMotorControl2(self.vehicle_id,self.vehicle_info.driving_wheel_joint_id,p.POSITION_CONTROL,targetVelocity=0.3,targetPosition=action[0],force=40) #action[0]
