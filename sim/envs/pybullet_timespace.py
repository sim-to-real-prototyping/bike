import math
from easydict import EasyDict
import pybullet as p
import numpy as np


class EnvHistory:
  def __init__(self,time_interval_length_s):
    self.prev_gravity_angles = None
    self.prev_local_axis = []
    self.meas_loc_xyz = []
    self.pos_history_len = 10
    self.dist = 0
    self.time_interval_length_s = time_interval_length_s
    self.missing_history_test = 0

  def add_measurement_point_location(self, xyz):
    if self.meas_loc_xyz != []:
      dst = math.hypot(xyz[1] - self.meas_loc_xyz[-1][1], xyz[0] - self.meas_loc_xyz[-1][0],xyz[2] - self.meas_loc_xyz[-1][2])
      self.dist += dst
    self.meas_loc_xyz.append(xyz)
    if len(self.meas_loc_xyz) > self.pos_history_len:
      self.meas_loc_xyz = self.meas_loc_xyz[1:]

  def add_local_axis(self, local_axis):
    self.prev_local_axis.append(local_axis)
    if len(self.prev_local_axis) > 30:
      self.prev_local_axis = self.prev_local_axis[1:]

  def get_local_axis(self, ts_ago):
    if len(self.prev_local_axis) > ts_ago:
      return self.prev_local_axis[ts_ago]
    
    return self.prev_local_axis[-1]

  def get_xyz(self, steps_ago):
    if len(self.meas_loc_xyz) > steps_ago:
      return self.meas_loc_xyz[steps_ago]

    return self.meas_loc_xyz[-1]

  def get_xyz_velocity_ms(self, from_steps_ago, to_steps_ago):
    if len(self.meas_loc_xyz) > from_steps_ago:
      f = np.array(self.meas_loc_xyz[-(from_steps_ago+1)])
    else:
      self.missing_history_test += 1
      if self.missing_history_test > 30:
        print("missing history")

      return np.array([0, 0, 0])
    
    if len(self.meas_loc_xyz) > to_steps_ago:
      t = np.array(self.meas_loc_xyz[-(to_steps_ago+1)])
    else:
      self.missing_history_test += 1
      if self.missing_history_test > 30:
        print("missing history")
      return np.array([0, 0, 0])

    speed = f-t
    speed_ms = speed/self.time_interval_length_s
    return speed_ms

class PybulletTimespace:
  def __init__(self, vehicle_info, vehicle_id, time_interval_length_s, gravity_f):
    self.gravity_f = gravity_f
    self.vehicle_info = vehicle_info
    self.vehicle_id = vehicle_id
    self.time_interval_length_s = time_interval_length_s
    self.hist = EnvHistory(self.time_interval_length_s)

  @staticmethod
  def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    if np.absolute(vector).sum() < 1e-10:
      print("Warning unit vector taken from zero vec. Small epsilon added to avoid exceptions")
      return vector / np.linalg.norm(np.array(vector) + 0.000001)
    return vector / np.linalg.norm(vector)

  def link_world_position(self, code):
    li = self.vehicle_info.link_info
    id = li[code].id
    #id = self.code_to_id(code, self.vehicle_id)
    if id == -1:
      raise Exception("Base object was behaving unexpectedly. So base object should not be used in position, orientation or force calculations")
    else:
      world_pos_center = p.getLinkState(self.vehicle_id, id)[0]

    #world_pos_center, _ = [state[i] for i in [0,1]]
    if np.isnan(world_pos_center).any():
      raise Exception("World position contains nan values")

    return world_pos_center

  def front_direction(self):
    x,y,z = self.link_world_position(self.vehicle_info.measurement_point)
    xf,yf,zf = self.link_world_position(self.vehicle_info.measurement_point_front)

    return xf-x,yf-y,zf-z

  def _direction_vec(self, direction_code, center):
    world_pos = self.link_world_position(direction_code)
    direction_vec = np.array(world_pos) - np.array(center)
    return self.unit_vector(direction_vec)

  def direction_vec_front(self):
    world_pos_center = self.link_world_position(self.vehicle_info.measurement_point)
    return self._direction_vec(self.vehicle_info.measurement_point_front, world_pos_center)

  def local_to_glob_right_front_up(self):
    world_pos_center = self.link_world_position(self.vehicle_info.measurement_point)

    vec_front = self._direction_vec(self.vehicle_info.measurement_point_front, world_pos_center)
    vec_right = self._direction_vec(self.vehicle_info.measurement_point_right, world_pos_center)
    vec_up = -1*self._direction_vec(self.vehicle_info.measurement_point_below, world_pos_center)
    
    return EasyDict({"right":vec_right, "front":vec_front, "up":vec_up})

  @staticmethod
  def _assert_reference_link_angles_correct(up, front, right):
    """
    There was a bug. Using base link as part of angle calculation did not give 90 degrees angles with front, up and right. 
    This method is to verify such issue will be catched earlier next time.
    """
    def assert_angle(a,b):
      dot_product = np.dot(a,b)
      assert np.absolute(np.arccos(dot_product) - math.pi/2) < 0.001

    if np.random.rand() < 0.01:
      assert_angle(up, front)
      assert_angle(up, right)
      assert_angle(right, front)

  def global_direction_in_local_coordinate(self, global_direction):
    coordinate_matrix = self.local_to_glob_right_front_up()
    self._assert_reference_link_angles_correct(coordinate_matrix.up, coordinate_matrix.front, coordinate_matrix.right)
    local_direction = self._projection_to_matrix_coord(\
      [coordinate_matrix.front, coordinate_matrix.right, coordinate_matrix.up], global_direction)
    return local_direction

  def magnetic_north(self):
    # magnetic north is along positive x-axis
    north = np.array([1,0,0])
    local_direction = self.global_direction_in_local_coordinate(north)
    return self.unit_vector(local_direction)
  def gravitation_in_local_coordinate(self):
    """
    Returns gravitation direction from measurement point. Returned direction is 3d vector. Values are shperical unit 
    coordinates relative to y-axis [0,1,0]
    """
    gravity_glo = np.array([0,0,-1])
    local_direction = self.global_direction_in_local_coordinate(gravity_glo)
    #return self.unit_vector(local_direction)
    return local_direction

  @staticmethod
  def _projection_to_matrix_coord(matrix, vec2):
    Q = np.array(matrix).T
    return np.linalg.solve(Q, vec2)
    
  @staticmethod
  def plane_angles_xz_xy_yz(force_vec):
    x,y,z=force_vec
    xza = math.atan2(x,z)
    xya = math.atan2(x,y)
    yza = math.atan2(y,z)

    return xza, xya, yza

  @staticmethod
  def angle_diff(a1, a2):
    da = a1 - a2
    if da > math.pi:
      da= da - 2*math.pi # Should end up being positive
    elif da < -math.pi:
      da = da + 2*math.pi # Should end up being negative

    assert abs(da) < math.pi 
    return da

  def get_local_acceleration_g(self):
    over_ts = 2
    speed_current = self.hist.get_xyz_velocity_ms(0,over_ts)
    speed_prev = self.hist.get_xyz_velocity_ms(over_ts,2*over_ts)

    acceleration_ts_ms2 = (speed_current-speed_prev) / self.time_interval_length_s / over_ts
    #print(self.gravity_f, "+ self.gravity_f")
    acceleration_ts_ms2[2] = acceleration_ts_ms2[2] + self.gravity_f
    acceleration_g = acceleration_ts_ms2 / self.gravity_f
    acceleration_g_loc = self.global_direction_in_local_coordinate(acceleration_g)

    return acceleration_g_loc

  def calculate_angle_deg_per_s(self, direction):
    def angle_to_deg_per_s(ang_rad):
      rad_to_deg = 57.2957795 # 180/PI
      return ang_rad / self.time_interval_length_s * rad_to_deg

    def axis_info(direction, steps_delta):
      if direction == "yaw":
        return self.hist.get_local_axis(steps_delta).right, 0, 1
      elif direction == "pitch":
        return self.hist.get_local_axis(steps_delta).front, 2, 0
      elif direction == "roll":
        return self.hist.get_local_axis(steps_delta).up, 1, 2
      else:
        raise Exception("ei implementoitu")          
        
    steps_delta = 5
    axis_vec, axis1, axis2 = axis_info(direction, steps_delta)

    ca = self.local_to_glob_right_front_up()
    proj = self._projection_to_matrix_coord([ca.front, ca.right, ca.up], axis_vec)
    plane = self.unit_vector([proj[axis1],proj[axis2]])

    ang_rad = math.atan2(*plane)
    deg_per_s = angle_to_deg_per_s(ang_rad) / steps_delta
    return deg_per_s

  def magnetometer_vals(self):
    front_dir = self.direction_vec_front()
    north_dir = front_dir[0]
    west_dir = front_dir[1]
    leng = math.sqrt(west_dir**2 + north_dir**2) 
    north_dir /= leng
    west_dir /= leng
    compas_angle = math.atan2(west_dir, north_dir)

    return compas_angle, north_dir, west_dir

  def save_state(self):
    prev_gravity_local = self.gravitation_in_local_coordinate()
    self.hist.prev_gravity_angles = self.plane_angles_xz_xy_yz(prev_gravity_local)
    self.hist.add_local_axis(self.local_to_glob_right_front_up())
    # Info needed by accelerometer
    xyz = self.link_world_position(self.vehicle_info.measurement_point)
    self.hist.add_measurement_point_location(xyz)


class IMU:
  def __init__(self, spatial):
    self.spatial = spatial

  def values(self):
    # https://github.com/Seeed-Studio/Grove_IMU_10DOF_v2.0/blob/master/examples/IMU_10DOF_V2_Test/IMU_10DOF_V2_Test.ino#L193
    # accelerator & gryo: https://files.seeedstudio.com/wiki/Grove-IMU_10DOF/res/MPU-9250A_Product_Specification.pdf
    # pressure sencor: https://files.seeedstudio.com/wiki/Grove-Barometer_Sensor-BMP280/res/Grove-Barometer_Sensor-BMP280-BMP280-DS001-12_Datasheet.pdf
    # gyroscopes: 3,    ±250°/sec
    # accelerometer: 3, ±2g
    # magnetometer: 3,  ±4800uT
    # barometer: 1, 300 ~ 1100hPa

    yaw = self.spatial.calculate_angle_deg_per_s("yaw")

    roll = self.spatial.calculate_angle_deg_per_s("roll")
    pitch = self.spatial.calculate_angle_deg_per_s("pitch")
    acceleration_g = self.spatial.get_local_acceleration_g()
    acceleration_measure_g = np.clip(acceleration_g, -2,2)
    compas_angle, north_dir, west_dir = self.spatial.magnetometer_vals()
    
    res = EasyDict({"front_sim_only":self.spatial.front_direction(), \
      "gyro_d_per_s": {"pitch":pitch, "yaw": yaw, "roll": roll},\
      "acceleration_g": {"x": acceleration_measure_g[0], "y":acceleration_measure_g[1],"z": acceleration_measure_g[2]},\
      "magnetometer" : {"north": north_dir, "west": west_dir, "angle": compas_angle}})

    return res
