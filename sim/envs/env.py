import math
from easydict import EasyDict
import gym
from gym import spaces
import numpy as np
from sim.envs.pybullet_env import PyBulletEnv
from sim.vehicle_info import VehicleInfo
from sim.driver import DriverTrain
import uuid
import time
from sim.direction_calculations import DirectionEstimator

rad_to_deg = 57.2957795

class BikeGym(gym.Env, PyBulletEnv):
  metadata = {'render.modes': ['human']}
  def __init__(self, driver, debug, gui, stage, include_randome_forces, take_init_steps=True):
    super(BikeGym, self).__init__()
    self.take_init_steps = take_init_steps
    self.env_id = uuid.uuid4()
    print(f"Environment Id {self.env_id}. Stage {stage}")
    #print("Environment Id", np.random.randint(0,100000))
    # Actions of the format of rotor position. 
    self.action_space = spaces.Box(low=np.array([-1]), high=np.array([1]), dtype=np.float16)
    # Current and previous rotor position
    self.observation_space = spaces.Box(low=-1, high=1, shape=([8]), dtype=np.float16)
    #self.observation_space = spaces.Box(low=np.array([-1, -1]), high=np.array([1, 1]), dtype=np.float16)

    #self.prev_rotor_angle = None
    self.done = False
    self.debug = debug
    self.driver = driver
    self.gui = gui
    self.setUpPyBullet(gui, include_randome_forces)
    #self.target = self.driver.get_action()
    self.step_count = 0
    self.prev_obs = []
    self.cumulative_roll = 0
    self.counter = 0
    self.stage = stage

  # TODO: Varmista, että reset toimii oikein uusien _next_obs arvojen kanssa
  def reset(self):
    def verify_imu_is_calibrated():
      _,y,z = self.imu.values().front_sim_only
      if abs(y) > 0.005 or abs(z) > 0.005:
        raise Exception(f"verify IMU unit is calibrated well. Limit {0.005} but was {y} and {z}")

    self.done = False
    self.step_count = 0
    #target = self.driver.get_action()
    obs =  self.reset_pybullet()
    target = self.driver.reset(None, None)
    if self.take_init_steps:
      initial_step_cnt = int(self.steps_per_sec/self.simulations_per_step*0.35)
      default_target = EasyDict({"force":VehicleInfo.limits.force.max})
      for i in range(initial_step_cnt):
        self._take_action([0], default_target)
        obs = self.next_observation()
        if i == 15:
          verify_imu_is_calibrated()

    state, preprocessed = self._normalize_state(obs, target, [])
    obs.roll_amplifier = preprocessed.roll_amplifier
    self.prev_obs = [obs]
    #self.prev_obs.roll_amplifier = preprocessed.roll_amplifier
    return state

  def _get_reward(self, controller_action, driver_action, obs):
    # Worth noticing that reward calculation is denormalizing its own data
    # -> is not using the same normalization logic as is used for model input data
    
    target_roll = driver_action.roll
    gryo_roll = obs.estimations.gryo.roll / rad_to_deg
    roll_penalty = abs(target_roll-gryo_roll)**1.5
    roll_penalty *= -2
    roll_penalty = np.clip(roll_penalty, -2,2)

    #print(target_roll,gryo_roll,roll_penalty, "target_roll-gryo_roll")

    
    #roll = self.direction_estimates.get_actual_roll()
    #roll = DirectionEstimator.get_gravitation_roll(-obs.estimations.default)
    #roll = DirectionEstimator.get_gravitation_roll(-obs.estimations.acceleration)
    #xyz = obs.estimations.default
    #roll_penalty = -abs(xyz[1]-target_roll)
    #roll_penalty *= 3
    
    y_acceleration = abs(obs.estimations.acceleration[1])
    tilt_penalty = -9*y_acceleration if y_acceleration > 0.25 else 0
    reward = roll_penalty + tilt_penalty # + speed_penalty + roll_penalty

    return reward

  def _normalize_state(self, obs, target, prev_obs):
    normalize_roll = lambda x: x/VehicleInfo.limits.roll_gryo.max

      #return math.sqrt(abs(obs_roll - math.pi/2) / (math.pi/2))*2 - 1
       
    norm_speed = lambda x: (x/1)*2-1
    #roll_amplifier = get_roll_amplifier(obs.roll)

    roll_amplifier = DirectionEstimator.get_gravitation_roll(-obs.estimations.acceleration)
    #roll_dist = DirectionEstimator.get_gravitation_roll(-obs.estimations.dist)

    #roll_amplifier = self.get_roll_custom(obs)
    #roll_amplifier = self.direction_estimates.get_actual_roll()
    #roll_amplifier = DirectionEstimator.get_gravitation_roll(self.spatial.gravitation_in_local_coordinate() )

    
    if prev_obs == []:
      prev_roll_amplifier = roll_amplifier
      prev_ob = obs
      earlier_ob1 = obs
    else:
      prev_ob = prev_obs[-1]
      prev_roll_amplifier = prev_ob.roll_amplifier
      
      earlier_ind1 = 4 if len(prev_obs) > 4 else  0
      earlier_ob1 = prev_obs[earlier_ind1]

    speed_change = obs.speed- prev_ob.speed
    speed_change *= 8
    roll_change_prev = roll_amplifier - prev_roll_amplifier
    roll_change_prev *= 6
    roll_change_prev = np.clip(roll_change_prev, -3, 3)


    dr_wheel_change = obs.dr_wheel - prev_ob.dr_wheel
    dr_wheel_change *= 5
    
    #dr_wheel_change_earlier = prev_ob.dr_wheel - earlier_ob1.dr_wheel
    #dr_wheel_change_earlier *= 1.5
    roll_amplifier = np.clip(roll_amplifier, -1.5,1.5)

    #acceleration_quantity = (obs.acceleration_quantity - 1)*2
    #acceleration_quantity = np.clip(acceleration_quantity, -1.5,1.5)
    y_coord = obs.estimations.acceleration[1] #* 2
    z_coord = obs.estimations.acceleration[2] #* 2
    #roll = obs.estimations.gryo.roll/150

    
    gryo_roll = obs.estimations.gryo.roll / rad_to_deg
    roll = 1*np.sign(gryo_roll)*abs(gryo_roll)**0.5

    roll_w = 0.1
    self.cumulative_roll = roll*roll_w+(1-roll_w)*self.cumulative_roll
    #print(self.cumulative_roll, "self.cumulative_rollself.cumulative_roll")

    #print(tmp_roll, "rollroll", gryo_roll)

    if prev_obs == []:
      prev_roll = roll
    else:
      prev_ob = prev_obs[-1]
      prev_roll = prev_ob.estimations.gryo.roll/150


    #print(y_coord, "y_coordy_coord", z_coord)

    preprocessed = EasyDict({"roll_amplifier": roll_amplifier})
    
    # state = [roll_amplifier, norm_speed(obs.speed), obs.dr_wheel, normalize_roll(target.roll), \
    #   roll_change_prev, dr_wheel_change, dr_wheel_change_earlier] # , acceleration_quantity
    state = [y_coord, z_coord, norm_speed(obs.speed), obs.dr_wheel, \
      normalize_roll(target.roll),dr_wheel_change, roll, self.cumulative_roll] # , acceleration_quantity
    return state, preprocessed

  def _denormalize_action(self, action):
    driving_wheel = [action[0]]
    return driving_wheel
  
  def _is_done(self, reward):
    return reward < -50 or self.step_count > 1000

  def step(self, action):
    self.counter += 1

    if self.done and self.debug:
      print("ALERT: is done")

    action = self._denormalize_action(action)

    target_old = self.driver.action

    self._take_action(action, target_old)

    self.randome_forces.step()
    obs = self.next_observation()

    roll_gryo = obs.estimations.gryo.roll / rad_to_deg
    roll_to_gravity = DirectionEstimator.get_gravitation_roll(self.spatial.gravitation_in_local_coordinate() )
    target_new = self.driver.update_action(roll_gryo, roll_to_gravity)

    reward = self._get_reward(action, target_old, obs)
    self.done = self._is_done(reward)

    state, preprocessed = self._normalize_state(obs, target_new, self.prev_obs, )
    obs.roll_amplifier = preprocessed.roll_amplifier

    self.prev_obs.append(obs)
    if len(self.prev_obs) > 30:
      self.prev_obs = self.prev_obs[1:]

    self.step_count += 1
    return state, reward, self.done, {}

  def render(self, mode='human', close=False):
    # Render the environment to the screen
    _, _, pos, _ = self.obs()

    print(f"current pos: {pos}")

def env_train_fn(stage=None,debug=False, gui=False):
  return BikeGym(DriverTrain(),debug, gui, stage) 

