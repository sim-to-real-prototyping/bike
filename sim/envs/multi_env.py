from sim.envs.env import BikeGym
from easydict import EasyDict
import itertools
import time
import gym
from OpenAI.vec_env.subproc_vec_env import SubprocVecEnv
#from OpenAI.spinup.algos.pytorch.ddpg import ddpg
#import OpenAI.spinup.algos.pytorch.ddpg.core as core
import torch
from sim.driver import DriverTrain
import numpy as np



def env_train_fn_multi(include_randome_forces):
  def func(stage=None,num_envs=1,debug=False, gui=False):
    return MultiprocessBikeGym(num_envs, stage, include_randome_forces) 

  return func


def make_train_env(stage,include_randome_forces):
  def _thunk():
    return BikeGym(DriverTrain(),debug=False, gui=False, stage=stage, include_randome_forces=include_randome_forces)

  return _thunk

class MultiprocessBikeGym(gym.Env):
  def __init__(self, num_envs, stage, include_randome_forces):
    super(MultiprocessBikeGym, self).__init__()
    def multi_sample(action_space, num_envs):
      def random_sample():
        return np.array([action_space.sample() for _ in range(num_envs)])

      return random_sample

    """
    def make_env(i, env_vars, headless):
      def _thunk():
        env = Env(env_vars, i, not(not headless and i == 0))
        return env

      return _thunk

    env_vars = EnvVars(args)
    agent = Agent(env_vars)
    env_vars.update_mode_info(agent)

    envs = [make_env(i, env_vars, env_vars.headless) for i in range(args.num_envs)]
    envs = SubprocVecEnv(envs)
    """
    self.num_envs = num_envs

    #if num_envs == 1:
      #env_train_fn(stage=None,debug=False, gui=False)
    envs = [make_train_env(stage,include_randome_forces) for i in range(num_envs)]
    envs = SubprocVecEnv(envs)

    self.envs = envs

    self.observation_space = envs.observation_space
    self.action_space = envs.action_space

    setattr(self.action_space, "multi_sample", multi_sample(self.action_space, num_envs))

  def reset(self):
    return self.envs.reset()

  def reset_single(self, env_index):
    return self.envs.reset_single(env_index)

  def step(self, action):
    return self.envs.step(action)

    """
    miten käytetään env? Miten mäpätään Gym.Env?:
    Käytetty seuraavasti:
    * state = self.envs.reset()
    * next_state, done, step_info = self.envs.step(action_dict)
    Sisältää myös tiedot:
    * num_envs
    * observation_space
    * action_space

    """
    #TODO: eli pitää lisätä sample joka palauttaa num_envs*action_space?
