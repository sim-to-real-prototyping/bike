from easydict import EasyDict
import numpy as np
import math
from sim.envs.pybullet_timespace import PybulletTimespace

class DirectionEstimator:
  def __init__(self, spatial, vehicle_info):
    self.vehicle_info= vehicle_info
    self.spatial = spatial
    self.estimation = None
    self.acceleration_error = np.array([0,0,0])
    self.estimation_error = np.array([0,0,0])
    self.estimation_error_intermediate = np.array([0,0,0])
    self.acceleration_unit_prev = None
    self.estimation_before = None
    self.debug = False
    self.estimations = EasyDict({"default": None, "dist": None, "gryo": None})


  def init_val(self, acc, gryo):
    if self.estimation is None:
      self.estimation = acc
      self.estimations.dist = acc
      self.estimations.default = acc
      self.acceleration_avg = np.array(acc)
      self.gryo_avg = EasyDict(gryo.copy())

  def get_new_estimate(self, gryo, old_est, time_interval_length_s):
    rad_to_deg = 57.2957795

    gryo_g = gryo*time_interval_length_s
    gryo_g_rad = gryo_g / rad_to_deg
    new_est = old_est + gryo_g_rad

    return new_est

  @staticmethod
  def get_yaw(direction_vector):
    return DirectionEstimator.get_proj_info([0,1], direction_vector)
  @staticmethod
  def get_pitch(direction_vector):
    return DirectionEstimator.get_proj_info([2,0], direction_vector)
  @staticmethod
  def get_roll(direction_vector):
    return DirectionEstimator.get_proj_info([2,1], direction_vector)
  @staticmethod
  def get_gravitation_roll(gravity_direction):
    # angle is calculated based on axis' positive direction. 
    # Gravitation is downward. In our solution that is negative direction. Thus direction vector is turned the opposite.
    return DirectionEstimator.get_proj_info([2,1], -gravity_direction)

  def get_actual_roll(self):
    # Returns roll for reward calculation etc. Roll calculation has direct access to pybullet simulator
    li = self.vehicle_info.link_info
    center_world_pos_center = self.spatial.link_world_position(self.vehicle_info.measurement_point)
    right_world_pos_center = self.spatial.link_world_position(self.vehicle_info.measurement_point_right)
    z_diff = center_world_pos_center[2] - right_world_pos_center[2]
    z_diff_sign = np.sign(z_diff)
    z_diff = abs(z_diff)
    z_diff /= 0.05
    z_diff = z_diff_sign*z_diff
    #z_diff =  z_diff_sign*math.sqrt(z_diff)
    return z_diff

  @staticmethod
  def get_proj_info(axis, direction_vector):
    xy_proj = direction_vector[np.array(axis)]
    xy_proj_len = math.hypot(*xy_proj)
    ang = math.atan2(xy_proj[1], xy_proj[0])
    return ang  

  def direction_vec_to_ang_ypr(self, direction_vec):
    yaw = self.get_yaw(direction_vec)
    pitch = self.get_pitch(direction_vec)
    roll = self.get_roll(direction_vec)
    return [yaw,pitch,roll]


  def _update_estimation_vec(self, est_dir, acc_dir):
    #print(est_dir, acc_dir, "est_dir, acc_direst_dir, acc_dir")
    w_est = 15
    w_acc = 1
    est_dir = np.array(est_dir)
    acc_dir = np.array(acc_dir)
    return (w_est*est_dir + w_acc*acc_dir)/(w_est + w_acc)
    #return est_dir*0.8+acc_dir*0.2

  def _update_estimation_dist_vec(self, est_dir, acc_dir, dist):
    w_acc = 1
    w_dist = abs(1-dist)*4
    w_est = 15*(1+w_dist)
    est_dir = np.array(est_dir)
    acc_dir = np.array(acc_dir)
    return (w_est*est_dir + w_acc*acc_dir)/(w_est + w_acc)

  def force_estimations(self, state, time_interval_length_s):
    ag = state.acceleration_g
    acc_g1 = np.array([ag.x, ag.y, ag.z])
    acc_quantity = math.hypot(*acc_g1)
     # TODO: tän voi korvata unit_vect funkkarilla
    acc_g = acc_g1/acc_quantity
    acc_down = -acc_g

    gryo = EasyDict(state.gyro_d_per_s)

    estimation = self._update_estimation(gryo, acc_g, time_interval_length_s, acc_quantity, acc_g1)
    #print(estimation, "estimation")

    return estimation, acc_quantity


  def _update_estimation(self, gryo, acceleration_direction, time_interval_length_s, acceleration_quantity, acceleration):
    def update_acc_avg(old_avg, acc):
      avg_w = 0.1
      return old_avg*(1-avg_w)+np.array(acc)*avg_w

    def update_gryo_avg(avg, gryo):
      avg_w = 0.1

      avg.roll = avg.roll*(1-avg_w)+gryo.roll*avg_w
      avg.pitch = avg.pitch*(1-avg_w)+gryo.pitch*avg_w
      avg.yaw = avg.yaw*(1-avg_w)+gryo.yaw*avg_w
      return avg
      


    self.acceleration_unit_prev = acceleration_direction.copy()
    self.init_val(acceleration_direction, gryo)
    #est_ang_ypr = self.direction_vec_to_ang_ypr(self.estimation)
    est_ang_ypr = self.direction_vec_to_ang_ypr(self.estimations.default)

    yaw_est1 = self.get_new_estimate(gryo.yaw, est_ang_ypr[0], time_interval_length_s)
    pitch_est1 = self.get_new_estimate(gryo.pitch, est_ang_ypr[1], time_interval_length_s)
    roll_est1 = self.get_new_estimate(gryo.roll, est_ang_ypr[2], time_interval_length_s)
    est_dir = self.ang_to_direction_vec(yaw_est1,pitch_est1,roll_est1)
    self.estimation_before = est_dir.copy()

    self.acceleration_avg = update_acc_avg(self.acceleration_avg, acceleration)
    est_dir = self._update_estimation_vec(est_dir,  PybulletTimespace.unit_vector(self.acceleration_avg))

    self.estimation = est_dir
    self.estimations.default = est_dir
    self.estimations.acceleration = self.acceleration_avg
    if self.debug:
      self.print_errors()
    #return self.estimation

    dist_est = self.estimation_before.copy()
    dist_est = self._update_estimation_dist_vec(dist_est, acceleration_direction, acceleration_quantity)
    self.estimations.dist = dist_est

    self.gryo_avg = update_gryo_avg(self.gryo_avg, gryo)

    self.estimations.gryo_avg = self.gryo_avg
    self.estimations.gryo = gryo
    
    return self.estimations


  def estimation_direction_down(self):
    return -self.estimation
  
  def ang_to_direction_vec(self, ang_rad_y, ang_rad_p, ang_rad_r):
    sign_x = -1 if abs(ang_rad_y) > math.pi/2 else 1
    sign_z = -1 if abs(ang_rad_p) > math.pi/2 else 1
    #sign_y = -1 if abs(ang_rad_r) > math.pi/2 else 1
    sign_y = -1 if ang_rad_r < 0 else 1

    y_div_by_x = math.tan(ang_rad_y)
    x_div_by_z = math.tan(ang_rad_p) 
    y_div_by_z = math.tan(ang_rad_r) 
    z = 1
    x = abs(z * x_div_by_z)
    y = abs(z * y_div_by_z)

    return PybulletTimespace.unit_vector([sign_x*x,sign_y*y,sign_z*z])

  def _get_down_direction_error(self, new_estimation_vec, estimation_cumulative_vec):
    grav_in_loc = self.spatial.gravitation_in_local_coordinate()
    err = (np.array(new_estimation_vec) - np.array(grav_in_loc))**2
    return 0.01*err + 0.99*estimation_cumulative_vec

  def get_estimation_errors(self):
    est_vec_down = self.estimation_direction_down()
    self.acceleration_error = self._get_down_direction_error(-self.acceleration_unit_prev, self.acceleration_error)
    self.estimation_error = self._get_down_direction_error(est_vec_down, self.estimation_error)
    self.estimation_error_intermediate = self._get_down_direction_error(-self.estimation_before, self.estimation_error_intermediate)

    return self.acceleration_error, self.estimation_error_intermediate, self.estimation_error

  def print_errors(self):
    acc_error,est_err_interm, est_err = self.get_estimation_errors()
    print(acc_error, "acc_error", acc_error.sum())
    print(est_err_interm, "est_err_interm", est_err_interm.sum())
    print(est_err, "est_err", est_err.sum())
