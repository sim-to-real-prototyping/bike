from sim.vehicle_info import VehicleInfo
import random
import numpy as np
from easydict import EasyDict
from sim.keyboard_actions import KeyboardActions

class Driver:
  def update_action(self, roll_gryo, roll_to_gravity):
    """
    Palauttaa (force, angle) SI yksikäissä
    """
    raise Exception("Not implemented")

  def reset(self):
    """
    ilmoittaa driverille, että episode loppui
    """
    raise Exception("Not implemented")


class NoDriver(Driver):
  def get_action(self):
    return EasyDict({"roll": 0, "force":50})
  
  def reset(self):
    pass


"""
* tallenna uuteen target -luokkaan tieto target rollista
* aina kun nuolinäppäintä painettu oikealle niin lisää vähän target rolliin
* aina kun nuolinäppäintä painettu vasemmalle niin vähennä vähän targetista
* vie lukua joka iteraatiolla hieman lähemmäs nollaa.
"""

def decay_roll(roll):
  decay_factor = 0.003
  if abs(roll) < decay_factor:
    return 0
  
  sign = np.sign(roll)
  return -sign*decay_factor + roll


class DriverTrain2(Driver):
  def __init__(self):
      super().__init__()
      self.reset()
      self.counter = 0
      self.fixed_roll = True

  def get_action(self):
    if self.counter < 50:
      self.action.roll = 0 
    else:
      if self.fixed_roll:
        self.fixed_roll = False
        self.action.roll = self.action_orig.roll
      if self.counter%12 == 0:
        self.action.roll = decay_roll(self.action.roll)
    self.counter += 1
    return self.action
  
  def reset(self):
    """
    randome luku [-0.5,0.5] välillä. Kuvastaa kulmaa (custom unit)
    """
    l = VehicleInfo.limits
    target_roll = np.random.uniform(low=l.roll_gryo.min, high=l.roll_gryo.max, size=1)[0]
    force_max = l.force.max
    force_min =l.force.min
    target_force = np.random.uniform(low=force_min, high=force_max, size=1)[0]
    self.action_orig = EasyDict({"roll":target_roll, "force": target_force})
    self.action = EasyDict(self.action_orig.copy())
    return self.action

"""
* driverille tieto missä kulmassa ajoneuvo on. Tarkka kulma, ei estimaatti.
* updateta driver target vasta get_reward jälkeen ja ennen _normalize_state
** luo driver.action ja driver.update_action. Tallenna molemmat ja reward saa oldin ja muut uptadedin.
** logiikka: kallistus max/min rajoissa. Jos gryo roll alle x päässä tavoitteesta y stepin ajan, niin arvo uusi tavoite.
*** jos kallistuma iso, niin iso päinvastainen tavoite.
"""
class DriverTrain(Driver):
  def __init__(self):
      super().__init__()
      self.counter = EasyDict({"total": 0, "latest":0, "in_target":0})
      self.reset(None, None)
      
  def update_action(self, roll_gryo, roll_to_gravity):
    # If falling over, then rapid turn to other direction.
    min_in_target = 3
    target_diameter = 0.2
    max_roll_to_gravity = 0.3
    if abs(roll_to_gravity) > max_roll_to_gravity:
      l = VehicleInfo.limits
      new_gryo_roll = random.uniform(0.5,1)*l.roll_gryo.max
      self.action.roll = -np.sign(roll_to_gravity)*new_gryo_roll
      self.counter.in_target = 0
    
    if self.counter.total < 50:
      self.counter.total += 1
      return EasyDict({"roll":0, "force": self.action.force})
    else:
      self.counter.total += 1
      if abs(self.action.roll-roll_gryo) < target_diameter:
        self.counter.in_target += 1
      else:
        self.counter.in_target = 0

      if self.counter.in_target > min_in_target:
        self.reset(None, None)


      return self.action
  
  def reset(self, roll_gryo, roll_to_gravity):
    """
    randome luku [-0.5,0.5] välillä. Kuvastaa kulmaa (custom unit)
    """
    l = VehicleInfo.limits
    target_roll = np.random.uniform(low=l.roll_gryo.min, high=l.roll_gryo.max, size=1)[0]
    force_max = l.force.max
    force_min =l.force.min
    self.counter.in_target = 0
    target_force = np.random.uniform(low=force_min, high=force_max, size=1)[0]
    self.action = EasyDict({"roll":target_roll, "force": target_force})
    #self.action = EasyDict(self.action_orig.copy())
    return self.action

class DriverTest(Driver):
  def __init__(self):
    self.roll = 0
    self.action = EasyDict({"roll": 0, "force":200})
    self.counter = 0

  def update_action(self, roll_gryo, roll_to_gravity):
    #if roll_gryo is not None and roll_to_gravity is not None:

    parced = KeyboardActions.list_keyboard_actions()
    
    if "right" in parced:
      self.action.roll -= 0.05
    elif "left" in parced:
      self.action.roll += 0.05
    elif "up" in parced:
      self.action.force += 5
    elif "down" in parced:
      self.action.force -= 5
    
    #print(roll_to_gravity, "roll_to_gravityroll_to_gravity", self.action.roll)
    self.action.roll = decay_roll(self.action.roll)

    l = VehicleInfo.limits
    self.action.force = np.clip(self.action.force, l.force.min,l.force.max)
    self.action.roll = np.clip(self.action.roll, l.roll_gryo.min, l.roll_gryo.max)

     
    return self.action

  def reset(self, roll_gryo, roll_to_gravity):
    return self.update_action(roll_gryo, roll_to_gravity)

  

