from torch.optim import Adam
import OpenAI.spinup.algos.pytorch.td3.core as core
import torch
import numpy as np

class SetModel:
  """
  Olis hyvä yhdistää logsx.py tiedoston kanssa. Esim sisältää jo save_state methodin.
  """
  def __init__(self, output_dir, input_dir=None, device="cpu"):
    self.device = device
    self.input_dir = input_dir
    if self.input_dir is not None:
      self.input_dir = self.input_dir + "/pyt_save"
    self.output_dir = output_dir + "/pyt_save"
    #self.folder_name = folder_name
    #self.folder = f"/home/timoml/projects/bike/experiments/{self.folder_name}/pyt_save/" 

  def model(self, observation_space, action_space, **ac_kwargs):
    def get_acto_critic(observation_space, action_space, **ac_kwargs):
      #ac = core.MLPActorCritic(observation_space, action_space, **ac_kwargs)
      #return ac
      #folder_name = "1620486377" 
      model_path = self.input_dir + "/model.pt"
      model = torch.load(model_path)
      print("loading model from", model_path)
      #model = core.MLPActorCritic(env.observation_space, env.action_space)
      #model.train()
      return model
    if self.input_dir is not None:
      return get_acto_critic(observation_space, action_space, **ac_kwargs)
    else:
      return core.MLPActorCritic(observation_space, action_space, self.device, **ac_kwargs)

  def restore_optimizers(self, q_optimizer, pi_optimizer, q_lr=None, pi_lr=None):
    def update_lr(lr, optimizer):
      if lr is not None:
        for param_group in optimizer.param_groups:
          param_group['lr'] = lr

      return optimizer
    
    if self.input_dir is not None: 
      checkpoint = torch.load(self.input_dir+"/optimizers.pt")
      q_optimizer.load_state_dict(checkpoint['q_optimizer'])
      pi_optimizer.load_state_dict(checkpoint['pi_optimizer'])
      #start_step = checkpoint['start_step']
      
      q_optimizer = update_lr(q_lr, q_optimizer)
      pi_optimizer = update_lr(pi_lr, pi_optimizer)


    return q_optimizer, pi_optimizer

  def save_info(self,itr,rew,best,t):
    rew = str(np.round(rew,2))
    with open(self.output_dir+"/model.txt", "a") as file_object:
      # Append 'hello' at the end of file
      file_object.write(f"\n {itr},{rew},{best},{t}")


  def save_optimizers(self, q_optimizer, pi_optimizer):
    file_name = self.output_dir+"/optimizers.pt"
    torch.save({
        'q_optimizer': q_optimizer.state_dict(),
        'pi_optimizer': pi_optimizer.state_dict()
        }, file_name)

    

    return file_name
