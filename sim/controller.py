import math
import gym
from gym import spaces
import numpy as np
import pybullet as p
import time
from datetime import datetime

#from torch.nn.modules.container import T
#from OpenAI.spinup.algos.pytorch.ddpg import ddpg 
#import OpenAI.spinup.algos.pytorch.ddpg.core as core
from OpenAI.spinup.algos.pytorch.td3 import td3 
import OpenAI.spinup.algos.pytorch.td3.core as core
import torch
#from env import BikeGym, env_train_fn
from sim.envs.multi_env import env_train_fn_multi
from sim.envs.env import BikeGym
from torch.utils.tensorboard import SummaryWriter
from sim.set_up import SetModel
from sim.driver import DriverTest

"""
Muuta reward laskentaa
* jos painana nuolta vasemmalle niin:
** rewardissa, että acceleration vasemmalle hieman aikaa. 
** target acceleration abs vähenee jokaisella time stepillä
** lopulta target acceleration palautuu nollaan
*** miten nopeasti vähenee?
*** miten varmistan, ettei vain opi ignooraamaan targeettia jos ei nolla.
***** anna iso expotentiaalinen penalty mitä enemmin acc != target.
** tarvitseeko gryoa ollenkaan?
*** poistamaan tärinän
*** yaw ei pitäisi aiheuttaa ongelmia 



TODO: 
* käytä nykyistä default estimationista laskettua rollia
* tallenna uuteen target -luokkaan tieto target rollista
* aina kun nuolinäppäintä painettu oikealle niin lisää vähän target rolliin
* aina kun nuolinäppäintä painettu vasemmalle niin vähennä vähän targetista
* vie lukua joka iteraatiolla hieman lähemmäs nollaa.
* anna controllerille inputiksi target roll ja yksi historia
"""


"""

* driverille tieto missä kulmassa ajoneuvo on. Tarkka kulma, ei estimaatti.
* updateta driver target vasta get_reward jälkeen ja ennen _normalize_state
** luo driver.get_action ja driver.update_action. Tallenna molemmat ja reward saa oldin ja muut uptadedin.
** logiikka: kallistus max/min rajoissa. Jos gryo roll alle x päässä tavoitteesta y stepin ajan, niin arvo uusi tavoite.
*** jos kallistuma iso, niin iso päinvastainen tavoite.


Kumulatiivinen roll?

mihin aika menee???
- On jo vähän liian hidas
"""


class Controller:
  def run(self):
    train = False
    #actor_critic=core.MLPActorCritic
    max_ep_len = 600
    epochs=30
    noise_clip = 0.2 # 0.5
    gamma = 0.99 # 0.5 # =0.99
    num_envs=4
    replay_size=int(1e6) 
    start_steps= 2100
    steps_per_epoch = 10000
    update_times=1000 # 50
    update_after=5000
    num_test_episodes=20
    batch_size = 512
    pi_lr=1e-5
    q_lr=1e-5
    update_every=2000

    artifact_folder_name = datetime.now().strftime("%Y%m%d%H%M%S")
    stored_folder_name = "20210720074347"
    input_dir = "/home/timoml/projects/bike/sim/experiments/"+stored_folder_name
    #input_dir=None
    output_dir = "/home/timoml/projects/bike/sim/experiments/%s"%artifact_folder_name
    include_randome_forces = False
    device = "cpu" # "cpu", "cuda"
    set_model = SetModel(output_dir, input_dir=input_dir, device=device)

    if train:
      #jatkokouluta tätä 1620754674, testaa tämä 1620757571
      writer_path = "sim/tensorboard_logs/"+artifact_folder_name
      writer = SummaryWriter(writer_path)
      logger_kwargs = {"output_dir": output_dir}
      #ddpg.ddpg(env_train_fn_multi, steps_per_epoch=2000, update_every=100, update_after=700, max_ep_len = 800, epochs=5, \
      # logger_kwargs=logger_kwargs, actor_critic = actor_critic)
      td3.td3(env_train_fn_multi(include_randome_forces), actor_critic=set_model, steps_per_epoch=steps_per_epoch, update_every=update_every, \
        update_after=update_after, max_ep_len=max_ep_len, epochs=epochs, logger_kwargs=logger_kwargs, num_envs=num_envs, \
        replay_size=replay_size, start_steps=start_steps, num_test_episodes=num_test_episodes, writer=writer,pi_lr=pi_lr,\
          q_lr=q_lr, batch_size=batch_size, noise_clip=noise_clip,update_times=update_times,device=device)
    else:
      # TODO: jotain menee pieleen. kun treenaan niin loss on melkein nolla. Kun testaan niin loss on 1-2
      max_ep_len = 2000 # 1000
      include_randome_forces = False
      take_init_steps = True

      env = BikeGym(DriverTest(), False, True, "UX", include_randome_forces, take_init_steps)
      folder_name = "20210720124526" # #20210719160017"
      model_path = f"/home/timoml/projects/bike/sim/experiments/{folder_name}/pyt_save/model3.pt"
      model = torch.load(model_path)
      #model = core.MLPActorCritic(env.observation_space, env.action_space)
      model.eval()
      # repr sisältää tyyppitiedon
      #time.sleep(14.02)
      obs = env.reset()

      actions = model.act(torch.as_tensor(obs, dtype=torch.float32))
      ab = 0

      while ab < max_ep_len:
        ab += 1
        obs, reward, done, _ = env.step(actions)
        actions = model.act(torch.as_tensor(obs, dtype=torch.float32))
        env.move_camera(env.vehicle_id, True)
        #if ab > 5:
        time.sleep(0.02)
        #if ab > 16: # 16, 100
        #  time.sleep(2.02)


        if done:
          print("oli done", actions, done, ab)
          break

