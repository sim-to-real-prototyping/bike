from easydict import EasyDict
import pybullet as p

class VehicleInfo:
  limits = EasyDict({"force":{"max":350, "min": 100}, "angle": {"min":-1, "max":1}, "roll_gryo":{"min":-0.5,"max":0.5}})
  driving_wheel_joint_name = "driwing_wheel_1_to_driwing_wheel_2"
  acceleration_joint_name = "wheel_to_shaft_rear"
  front_wheel_joint_name = "shaft_to_wheel_front"
  measurement_point_right = "measurement_point_right"
  wheel_bar_back_left = "wheel_bar_back_left"
  measurement_point = "measurement_point"

  link_names = ["wheel_rear", "wheel_bar_back_right", wheel_bar_back_left, "wheel_front", "wheel_bar_front_left", \
    "wheel_bar_front_right"]
 
  def __init__(self, vehicle_id):
    self.acceleration_joint_id = None
    #self.driving_wheel_joint = None
    self.front_wheel_joint_id = None

    self.driving_wheel_joint_id = None
    link_info = {}
    link_ids = []
    for i in range(p.getNumJoints(vehicle_id)):
      joint_name = p.getJointInfo(vehicle_id, i)[1].decode('UTF-8')
      if joint_name == self.driving_wheel_joint_name:
        self.driving_wheel_joint_id = i
      elif joint_name == self.acceleration_joint_name:
        self.acceleration_joint_id = i
      elif joint_name == self.front_wheel_joint_name:
        self.front_wheel_joint_id = i


      link_name = p.getJointInfo(vehicle_id, i)[12].decode('UTF-8')
      if link_name in self.link_names:
        link_ids.append(i)

      link_info[link_name] = {"name": link_name, "id": i}
      
      if link_name == self.measurement_point_right:
        self.measurement_point_right_id = i
      
    self.link_info = EasyDict(link_info)

    self.link_ids = link_ids

  @property
  def measurement_point_right(self):
    return self.measurement_point + "_right"

  @property
  def measurement_point_front(self):
    return self.measurement_point + "_front"

  @property
  def measurement_point_below(self):
    return self.measurement_point + "_below"


