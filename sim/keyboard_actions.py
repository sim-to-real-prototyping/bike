import pybullet as p
import time
from easydict import EasyDict

class KeyboardActions:
  chars = [EasyDict(x) for x in [{"k": "left", "v":65295, "n":"motor_roll_counter"},
                              {"k": "right", "v":65296, "n":"motor_roll_clock"},
                              {"k": "up", "v":65297, "n":"pitch_down"},
                              {"k": "down", "v":65298, "n":"pitch_up"}]]
  @staticmethod
  def __recent_actions(chars_mapping):
    def is_pressed_key(key_nr):
      def is_char(obj):
        return obj.v == key_nr
      return is_char

    actions = []
    count = 0
    keys = p.getKeyboardEvents()
    for k, v in keys.items():
      #if v & p.KEY_WAS_TRIGGERED:
      #print(list(filter(is_pressed_key(k), chars_mapping)), "oorforkrofkrokrfok")
      if v == 1 & p.KEY_IS_DOWN:
        pressed_key = list(filter(is_pressed_key(k), chars_mapping))
        if len(pressed_key) > 0:
          actions.append(pressed_key[0].k)

    return actions

  def list_keyboard_actions():
    keyboard_acts = KeyboardActions.__recent_actions(KeyboardActions.chars)
    
    return keyboard_acts
